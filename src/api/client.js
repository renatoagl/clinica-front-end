const API_URL = "http://localhost:8080";

// Post
export const post = async (url, payload) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(payload),
  };

  let response = await fetch(`${API_URL}/${url}`, requestOptions);

  return response.json();
};

// Get
export const get = async (url) => {
  const requestOptions = {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  };

  let link = `${API_URL}/${url}`;
  const response = await fetch(`${API_URL}/${url}`, requestOptions);

  try {
    return await response.json();
  } catch (error) {
    return error;
  }
};

// Put
export const put = async (url, payload) => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(payload),
  };

  let response = await fetch(`${API_URL}/${url}`, requestOptions);
  if (response.status === 204) {
    return { status: response.status };
  }
  return { status: response.status, data: await response.json() };
};

// Delete
export const deleteMethod = async (url) => {
  const requestOptions = {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
  };

  let response = await fetch(`${API_URL}/${url}`, requestOptions);
  return response.json();
};
