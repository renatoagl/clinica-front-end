export const initialConfigurationsState = {
  medicalRecords: [],
  medicines: [],
  searchTextFilter: "",
  loading: false,
};

export const configurationsReducer = (
  initialState = initialConfigurationsState,
  action
) => {
  switch (action.type) {
    /* Obtener todos los antecedentes */
    case "GET_MEDICAL_RECORDS":
      return { ...initialState, medicalRecords: [], loading: true };
    case "GET_MEDICAL_RECORDS_SUCCESS":
      return {
        ...initialState,
        medicalRecords: action.payload,
        loading: false,
      };
    case "GET_MEDICAL_RECORDS_FAILURE":
      return { ...initialState, medicalRecords: [], loading: false };

    /* Filtro de antecedente */
    case "SEARCH_TEXT_CHANGE":
      return { ...initialState, searchTextFilter: action.payload };
    case "SEARCH_TYPE":
      return { ...initialState, searchType: action.payload };

    /* Obtener todos los tipos de antecedentes */
    case "GET_TYPES":
      return { ...initialState, types: [], loading: true };
    case "GET_TYPES_SUCCESS":
      return { ...initialState, types: action.payload, loading: false };
    case "GET_TYPES_FAILURE":
      return { ...initialState, types: [], loading: false };

    /* Agregar o editar antecedentes */
    case "PERSIST_MEDICAL_RECORD":
      return {
        ...initialState,
        saved: false,
        savedError: false,
      };
    case "PERSIST_MEDICAL_RECORD_SUCCESS":
      return {
        ...initialState,
        saved: true,
        savedError: false,
      };
    case "PERSIST_MEDICAL_RECORD_FAILURE":
      return {
        ...initialState,
        saved: false,
        savedError: true,
      };
    case "BUILD_EMPTY_MEDICAL_RECORD":
      return { ...initialState, medicalRecord: null };
    case "SAVED_MEDICAL_RECORD":
      return {
        ...initialState,
        savedMedicalRecord: true,
        medicalRecord: action.payload,
      };
    case "RESET_SAVED_MEDICAL_RECORD":
      return { ...initialState, savedMedicalRecord: false };

    /* Obtener un usuario para editar */
    case "GET_MEDICAL_RECORD":
      return { ...initialState, medicalRecord: null, loading: true };
    case "GET_MEDICAL_RECORD_SUCCESS":
      return { ...initialState, medicalRecord: action.payload, loading: false };
    case "GET_MEDICAL_RECORD_FAILURE":
      return { ...initialState, medicalRecord: null, loading: false };

    /* Obtener todos los medicamentos */
    case "GET_MEDICINES":
      return { ...initialState, medicines: [], loading: true };
    case "GET_MEDICINES_SUCCESS":
      return { ...initialState, medicines: action.payload, loading: false };
    case "GET_MEDICINES:FAILURE":
      return { ...initialState, medicines: [], loading: false };

    /* Obtener un medicamento para editar */
    case "GET_MEDICINE":
      return { ...initialState, medicine: null, loading: true };
    case "GET_MEDICINE_SUCCESS":
      return { ...initialState, medicine: action.payload, loading: false };
    case "GET_MEDICINE_FAILURE":
      return { ...initialState, medicine: null, loading: false };

    /* Agregar o editar medicamento */
    case "PERSIST_MEDICINE":
      return {
        ...initialState,
        saved: false,
        savedError: false,
      };
    case "PERSIST_MEDICINE_SUCCESS":
      return {
        ...initialState,
        saved: true,
        savedError: false,
      };
    case "PERSIST_MEDICINE_FAILURE":
      return {
        ...initialState,
        saved: false,
        savedError: true,
      };
    case "BUILD_EMPTY_MEDICINE":
      return { ...initialState, medicine: null };
    case "SAVED_MEDICINE":
      return { ...initialState, savedMedicine: true, medicine: action.payload };
    case "RESET_SAVED_MEDICINE":
      return { ...initialState, savedMedicine: false };
  }
};
