import { post, get, put, deleteMethod } from "../../api/client";
import { NotificationManager } from "react-notifications";

export const getAllMedicalRecords = async (dispatch) => {
  try {
    dispatch({ type: "GET_MEDICAL_RECORDS" });
    const data = await get(`clinical-history-guides/all`);
    dispatch({ type: "GET_MEDICAL_RECORDS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_MEDICAL_RECORDS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const getMedicalRecordsFiltered = async (dispatch, paramsObject) => {
  try {
    dispatch({ type: "GET_MEDICAL_RECORDS" });
    const data = await get(
      `clinical-history-guides?text=${paramsObject.text}&type=${paramsObject.type}`
    );
    dispatch({ type: "GET_MEDICAL_RECORDS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_MEDICAL_RECORDS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const getMedicalRecord = async (dispatch, id) => {
  try {
    dispatch({ type: "GET_MEDICAL_RECORD" });
    const data = await get(`clinical-history-guides/${id}`);
    let typeAux = data.type;
    data.type = { label: typeAux, value: typeAux };
    dispatch({ type: "GET_MEDICAL_RECORD_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_MEDICAL_RECORD_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const persistMedicalRecord = async (dispatch, data, id) => {
  try {
    dispatch({ type: "PERSIST_MEDICAL_RECORD" });

    let response = null;
    if (id) {
      response = await put(`clinical-history-guides/${id}`, data);
      NotificationManager.success("Antecedente modificado correctamente.");
    } else {
      response = await post(`clinical-history-guides`, data);
      dispatch({ type: "SAVED_MEDICAL_RECORD", payload: response });
      NotificationManager.success("Antecedente creado correctamente.");
    }

    if (response.status === 204) {
      dispatch({ type: "PERSIST_MEDICAL_RECORD_SUCCESS" });
    }
  } catch (error) {
    console.log(error);
    NotificationManager.error("Se ha producido un error.");
    dispatch({ type: "PERSIST_MEDICAL_RECORD_FAILURE", payload: error });
  }
};

export const getTypes = async (dispatch) => {
  try {
    dispatch({ type: "GET_TYPES" });
    const response = await get(`clinical-history-guides/types`);
    let arrayAux = [];
    response.map((type) => {
      let objectAux = {};
      objectAux.label = type;
      objectAux.value = type;
      arrayAux.push(objectAux);
    });
    dispatch({ type: "GET_TYPES_SUCCESS", payload: arrayAux });
  } catch (error) {
    console.log(error);
    dispatch({ type: "GET_TYPES_FAILURE", payload: error });
  }
};

export const getAllMedicines = async (dispatch) => {
  try {
    dispatch({ type: "GET_MEDICINES" });
    const data = await get(`medicines/all`);
    dispatch({ type: "GET_MEDICINES_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_MEDICINES_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const getMedicinesFiltered = async (dispatch, paramsObject) => {
  try {
    dispatch({ type: "GET_MEDICINES" });
    const data = await get(`medicines?text=${paramsObject.text}`);
    dispatch({ type: "GET_MEDICINES_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_MEDICINES_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const getMedicine = async (dispatch, id) => {
  try {
    dispatch({ type: "GET_MEDICINE" });
    const data = await get(`medicines/${id}`);
    dispatch({ type: "GET_MEDICINE_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_MEDICINE_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const persistMedicine = async (dispatch, data, id) => {
  try {
    dispatch({ type: "PERSIST_MEDICINE" });

    let response = null;
    if (id) {
      response = await put(`medicines/${id}`, data);
      NotificationManager.success("Medicamento modificado correctamente.");
    } else {
      response = await post(`medicines`, data);
      dispatch({ type: "SAVED_MEDICINE", payload: response });
      NotificationManager.success("Medicamento creado correctamente.");
    }

    if (response.status === 204) {
      dispatch({ type: "PERSIST_MEDICINE_SUCCESS" });
    }
  } catch (error) {
    console.log(error);
    NotificationManager.error("Se ha producido un error.");
    dispatch({ type: "PERSIST_MEDICINE_FAILURE", payload: error });
  }
};
