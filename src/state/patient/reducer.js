export const initialPatientsState = {
  patients: [],
  searchTextFilter: "",
  loading: false,
};

export const patientsReducer = (
  initialState = initialPatientsState,
  action
) => {
  switch (action.type) {
    /* Obtener todos los pacientes */
    case "GET_PATIENTS":
      return { ...initialState, patients: [], loading: true };
    case "GET_PATIENTS_SUCCESS":
      return { ...initialState, patients: action.payload, loading: false };
    case "GET_PATIENTS_FAILURE":
      return { ...initialState, patients: [], loading: false };

    /* Filtro de paciente */
    case "SEARCH_TEXT_CHANGE":
      return { ...initialState, searchTextFilter: action.payload };

    /* Obtener un paciente para editar */
    case "GET_PATIENT":
      return { ...initialState, patient: null, loading: true };
    case "GET_PATIENT_SUCCESS":
      return { ...initialState, patient: action.payload, loading: false };
    case "GET_PATIENT_FAILURE":
      return { ...initialState, patient: null, loading: false };

    /* Agregar o editar paciente */
    case "PERSIST_PATIENT":
      return {
        ...initialState,
        saved: false,
        savedError: false,
      };
    case "PERSIST_PATIENT_SUCCESS":
      return {
        ...initialState,
        saved: true,
        savedError: false,
      };
    case "PERSIST_PATIENT_FAILURE":
      return {
        ...initialState,
        saved: false,
        savedError: true,
      };
    case "BUILD_EMPTY_PATIENT":
      return { ...initialState, patient: null };
    case "SAVED_PATIENT":
      return { ...initialState, savedPatient: true, patient: action.payload };
    case "RESET_SAVED_PATIENT":
      return { ...initialState, savedPatient: false };

    /* Reseteo de error para las notificaciones */
    case "RESET_PATIENT_ERROR":
      return {
        ...initialState,
        errorPatient: false,
      };

    default:
      return { ...initialState };
  }
};
