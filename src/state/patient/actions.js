import { post, get, put, deleteMethod } from "../../api/client";
import { NotificationManager } from "react-notifications";

export const getAllPatients = async (dispatch) => {
  try {
    dispatch({ type: "GET_PATIENTS" });
    const data = await get(`patients/all`);
    dispatch({ type: "GET_PATIENTS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_PATIENTS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const getPatientsFiltered = async (dispatch, paramsObject) => {
  try {
    dispatch({ type: "GET_PATIENTS" });
    const data = await get(`patients?text=${paramsObject.text}`);
    dispatch({ type: "GET_PATIENTS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_PATIENTS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const getPatient = async (dispatch, dni) => {
  try {
    dispatch({ type: "GET_PATIENT" });
    const data = await get(`patients/${dni}`);
    let sexAux = data.sex;
    data.sex = { label: sexAux, value: sexAux };
    dispatch({ type: "GET_PATIENT_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_PATIENT_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const persistPatient = async (dispatch, data, dni) => {
  try {
    dispatch({ type: "PERSIST_PATIENT" });

    let response = null;
    if (dni) {
      response = await put(`patients/${dni}`, data);
      NotificationManager.success("Paciente modificado correctamente.");
    } else {
      response = await post(`patients`, data);
      dispatch({ type: "SAVED_PATIENT", payload: response });
      NotificationManager.success("Paciente creado correctamente.");
    }

    if (response.status === 204) {
      dispatch({ type: "PERSIST_PATIENT_SUCCESS" });
    }
  } catch (error) {
    console.log(error);
    NotificationManager.error("Se ha producido un error.");
    dispatch({ type: "PERSIST_PATIENT_FAILURE", payload: error });
  }
};
