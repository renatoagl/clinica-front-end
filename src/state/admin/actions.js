import { post, get, put, deleteMethod } from "../../api/client";
import { NotificationManager } from "react-notifications";

export const getAllUsers = async (dispatch) => {
  try {
    dispatch({ type: "GET_USERS" });
    const data = await get(`users/all`);
    dispatch({ type: "GET_USERS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_USERS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const getUsersFiltered = async (dispatch, paramsObject) => {
  try {
    dispatch({ type: "GET_USERS" });
    const data = await get(`users?text=${paramsObject.text}`);
    dispatch({ type: "GET_USERS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_USERS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const getUser = async (dispatch, id) => {
  try {
    dispatch({ type: "GET_USER" });
    const data = await get(`users/${id}`);
    let specialityAux = data.speciality;
    let sexAux = data.sex;
    data.speciality = { label: specialityAux, value: specialityAux };
    data.sex = { label: sexAux, value: sexAux };
    dispatch({ type: "GET_USER_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_USER_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const persistUser = async (dispatch, data, id) => {
  try {
    dispatch({ type: "PERSIST_USER" });

    let response = null;
    if (id) {
      response = await put(`users/${id}`, data);
      NotificationManager.success("Usuario modificado correctamente.");
    } else {
      response = await post(`users`, data);
      dispatch({ type: "SAVED_USER", payload: response });
      NotificationManager.success("Usuario creado correctamente.");
    }

    if (response.status === 204) {
      dispatch({ type: "PERSIST_USER_SUCCESS" });
    }
  } catch (error) {
    console.log(error);
    NotificationManager.error("Se ha producido un error.");
    dispatch({ type: "PERSIST_USER_FAILURE", payload: error });
  }
};

export const getSpecialities = async (dispatch) => {
  try {
    dispatch({ type: "GET_SPECIALITIES" });
    const response = await get(`specialities/all`);
    response.map((e) => {
      e.id && delete e["id"];
      /* Ponemos label y value, luego eliminamos */
      e.label = e.name;
      e.value = e.name;
    });
    dispatch({ type: "GET_SPECIALITIES_SUCCESS", payload: response });
  } catch (error) {
    console.log(error);
    dispatch({ type: "GET_SPECIALITIES_FAILURE", payload: error });
  }
};
