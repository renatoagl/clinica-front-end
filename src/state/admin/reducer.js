export const initialUsersState = {
  users: [],
  searchTextFilter: "",
  loading: false,
};

export const usersReducer = (initialState = initialUsersState, action) => {
  switch (action.type) {
    /* Obtener todos los usuarios */
    case "GET_USERS":
      return { ...initialState, users: [], loading: true };
    case "GET_USERS_SUCCESS":
      return { ...initialState, users: action.payload, loading: false };
    case "GET_USERS_FAILURE":
      return { ...initialState, users: [], loading: false };

    /* Filtro de usuario */
    case "SEARCH_TEXT_CHANGE":
      return { ...initialState, searchTextFilter: action.payload };

    /* Obtener un usuario para editar */
    case "GET_USER":
      return { ...initialState, user: null, loading: true };
    case "GET_USER_SUCCESS":
      return { ...initialState, user: action.payload, loading: false };
    case "GET_USER_FAILURE":
      return { ...initialState, user: null, loading: false };

    /* Obtener las especialidades */
    case "GET_SPECIALITIES":
      return { ...initialState, specialities: [], loading: true };
    case "GET_SPECIALITIES_SUCCESS":
      return { ...initialState, specialities: action.payload, loading: false };
    case "GET_SPECIALITIES_FAILURE":
      return { ...initialState, specialities: [], loading: false };

    /* Agregar o editar usuario */
    case "PERSIST_USER":
      return {
        ...initialState,
        saved: false,
        savedError: false,
      };
    case "PERSIST_USER_SUCCESS":
      return {
        ...initialState,
        saved: true,
        savedError: false,
      };
    case "PERSIST_USER_FAILURE":
      return {
        ...initialState,
        saved: false,
        savedError: true,
      };
    case "BUILD_EMPTY_USER":
      return { ...initialState, user: null };
    case "SAVED_USER":
      return { ...initialState, savedUser: true, user: action.payload };
    case "RESET_SAVED_USER":
      return { ...initialState, savedUser: false };

    default:
      return { ...initialState };
  }
};
