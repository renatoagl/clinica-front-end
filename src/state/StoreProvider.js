import React, { useReducer } from "react";
import combineReducers from "react-combine-reducers";
import { layoutReducer, initialLayoutState } from "./layout";
import { usersReducer, initialUsersState } from "./admin";
import { patientsReducer, initialPatientsState } from "./patient";
import {
  configurationsReducer,
  initialConfigurationsState,
} from "./configuration";
import { initialMenuState, menuReducer } from "./menu";

const StoreStateContext = React.createContext();
const StoreDispatchContext = React.createContext();

export const useSelector = (state) => {
  const context = React.useContext(StoreStateContext);
  if (context === undefined) {
    throw new Error("useSelector must be used within a StoreStateProvider");
  }

  return context[state];
};

export const useDispatch = () => {
  const context = React.useContext(StoreDispatchContext);
  if (context === undefined) {
    throw new Error("useDispatch must be used within a StoreDispatchProvider");
  }

  return context;
};

export const StoreProvider = ({ children }) => {
  const [stateReducer, initialState] = combineReducers({
    layout: [layoutReducer, initialLayoutState],
    menu: [menuReducer, initialMenuState],
    users: [usersReducer, initialUsersState],
    patients: [patientsReducer, initialPatientsState],
    configurations: [configurationsReducer, initialConfigurationsState],
  });

  const [state, dispatch] = useReducer(stateReducer, initialState);

  return (
    <StoreStateContext.Provider value={state}>
      <StoreDispatchContext.Provider value={dispatch}>
        {children}
      </StoreDispatchContext.Provider>
    </StoreStateContext.Provider>
  );
};
