export const initialMenuState = {
  numberAppointments: 0,
  numberConsultations: 0,
  numberPatients: 0,
  patients: [],
};

export const menuReducer = (initialState = initialMenuState, action) => {
  switch (action.type) {
    case "COUNT_APPOINTMENTS":
      return { ...initialState, numberAppointments: 0 };
    case "COUNT_APPOINTMENTS_SUCCESS":
      return { ...initialState, numberAppointments: action.payload };
    case "COUNT_APPOINTMENTS_FAILURE":
      return { ...initialState, numberAppointments: 0 };

    case "COUNT_CONSULTATIONS":
      return { ...initialState, numberConsultations: 0 };
    case "COUNT_CONSULTATIONS_SUCCESS":
      return { ...initialState, numberConsultations: action.payload };
    case "COUNT_CONSULTATIONS_FAILURE":
      return { ...initialState, numberConsultations: 0 };

    case "COUNT_PATIENTS":
      return { ...initialState, numberPatients: 0 };
    case "COUNT_PATIENTS_SUCCESS":
      return { ...initialState, numberPatients: action.payload };
    case "COUNT_PATIENTS_FAILURE":
      return { ...initialState, numberPatients: 0 };

    case "GET_MENU_PATIENTS":
      return { ...initialState, patients: [] };
    case "GET_MENU_PATIENTS_SUCCESS":
      return { ...initialState, patients: action.payload };
    case "GET_MENU_PATIENTS_FAILURE":
      return { ...initialState, patients: [] };

    default:
      return { ...initialState };
  }
};
