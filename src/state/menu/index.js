import {
  countAppointmentsPerDoctor,
  countConsultationsPerDoctor,
  countPatientsPerDoctor,
  getMenuPatients,
} from "./actions";
import { initialMenuState, menuReducer } from "./reducer";
export {
  initialMenuState,
  menuReducer,
  countAppointmentsPerDoctor,
  countConsultationsPerDoctor,
  countPatientsPerDoctor,
  getMenuPatients,
};
