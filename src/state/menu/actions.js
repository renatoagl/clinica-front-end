import { post, get, put, deleteMethod } from "../../api/client";

export const countAppointmentsPerDoctor = async (dispatch) => {
  try {
    dispatch({ type: "COUNT_APPOINTMENTS" });
    const data = await get(`users/appointments-per-day`);
    console.log(data);
    dispatch({ type: "COUNT_APPOINTMENTS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "COUNT_APPOINTMENTS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const countConsultationsPerDoctor = async (dispatch) => {
  try {
    dispatch({ type: "COUNT_CONSULTATIONS" });
    const data = await get(`users/consultations-per-day`);
    dispatch({ type: "COUNT_CONSULTATIONS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "COUNT_CONSULTATIONS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const countPatientsPerDoctor = async (dispatch) => {
  try {
    dispatch({ type: "COUNT_PATIENTS" });
    const data = await get(`users/patients-per-doctor`);
    dispatch({ type: "COUNT_PATIENTS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "COUNT_PATIENTS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};

export const getMenuPatients = async (dispatch) => {
  try {
    dispatch({ type: "GET_MENU_PATIENTS" });
    const data = await get(`users/patient-appointments-per-doctor`);
    dispatch({ type: "GET_MENU_PATIENTS_SUCCESS", payload: data });
  } catch (error) {
    console.log(error);
    dispatch({
      type: "GET_MENU_PATIENTS_FAILURE",
      error: { code: 500, description: error },
    });
  }
};
