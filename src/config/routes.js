import LoginScreen from "views/login/LoginScreen";
import Menu from "views/menu-management/Menu";
import UserList from "views/admin-management/components/UserList";
import UserAdd from "views/admin-management/UserAdd";
import UserEdit from "views/admin-management/UserEdit";
import PatientList from "views/patient-management/components/PatientList";
import PatientAdd from "views/patient-management/PatientAdd";
import PatientEdit from "views/patient-management/PatientEdit";
import ConfigWindow from "views/config-management/ConfigWindow";
import ReportWindow from "views/report-management/ReportWindow";
import MedicalRecordList from "views/config-management/components/MedicalRecord/MedicalRecordList";
import MedicalRecordAdd from "views/config-management/components/MedicalRecord/MedicalRecordAdd";
import MedicalRecordEdit from "views/config-management/components/MedicalRecord/MedicalRecordEdit";
import MedicineList from "views/config-management/components/Medicine/MedicineList";
import MedicineAdd from "views/config-management/components/Medicine/MedicineAdd";
import MedicineEdit from "views/config-management/components/Medicine/MedicineEdit";
import HelpWindow from "views/help-management/HelpWindow";
import DiaryWindow from "views/diary-management/DiaryWindow";

export const privateRoutes = [
  {
    path: "/menu-management",
    component: Menu,
    isPrivate: true,
    title: "Menú Principal",
  },
  {
    path: "/reports-management",
    component: ReportWindow,
    isPrivate: true,
    title: "Reportes",
  },
  {
    path: "/user-management",
    component: UserList,
    isPrivate: true,
    title: "Administración - Gestión de usuarios",
  },
  {
    path: "/user-add",
    component: UserAdd,
    isPrivate: true,
    title: "Usuario - Adición",
  },
  {
    path: "/user-edit/:id",
    component: UserEdit,
    isPrivate: true,
    title: "Usuario - Edición",
  },
  {
    path: "/patient-management",
    component: PatientList,
    isPrivate: true,
    title: "Gestión de pacientes",
  },
  {
    path: "/patient-add",
    component: PatientAdd,
    isPrivate: true,
    title: "Paciente - Adición",
  },
  {
    path: "/patient-edit/:id",
    component: PatientEdit,
    isPrivate: true,
    title: "Paciente - Edición",
  },
  {
    path: "/config-management",
    component: ConfigWindow,
    isPrivate: true,
    title: "Configuración",
  },
  {
    path: "/config-management/medical-records",
    component: MedicalRecordList,
    isPrivate: true,
    title: "Configuración - Antecedentes Médicos",
  },
  {
    path: "/config-management/medical-records/:id",
    component: MedicalRecordEdit,
    isPrivate: true,
    title: "Antecedente Médico - Edición",
  },
  {
    path: "/config-management/medical-records-add",
    component: MedicalRecordAdd,
    isPrivate: true,
    title: "Antecedente Médico - Nuevo",
  },
  {
    path: "/config-management/medicines",
    component: MedicineList,
    isPrivate: true,
    title: "Configuración - Medicamentos",
  },
  {
    path: "/config-management/medicines/:id",
    component: MedicineEdit,
    isPrivate: true,
    title: "Medicamento - Edición",
  },
  {
    path: "/config-management/medicines-add",
    component: MedicineAdd,
    isPrivate: true,
    title: "Medicamento - Nuevo",
  },
  {
    path: "/help-management",
    component: HelpWindow,
    isPrivate: true,
    title: "Ayuda",
  },
  {
    path: "/diary-management",
    component: DiaryWindow,
    isPrivate: true,
    title: "Agenda",
  },
];

export const publicRoutes = [
  {
    path: "/login",
    component: LoginScreen,
    isPrivate: false,
  },
];
