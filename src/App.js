import "./App.css";
import React from "react";
import { privateRoutes, publicRoutes } from "./config/routes.js";
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from "react-router-dom";
import _ from "lodash";
import PrivateLayout from "./views/layout/PrivateLayout/PrivateLayout";
import PublicLayout from "./views/layout/PublicLayout/PublicLayout";
import { NotificationContainer } from "react-notifications";
import "react-notifications/lib/notifications.css";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          {_.map(privateRoutes, (route, key) => {
            const { component, path, title } = route;
            return (
              <Route
                exact
                title={title}
                path={path}
                key={key}
                render={(route) => {
                  return (
                    <PrivateLayout
                      component={component}
                      route={route}
                      title={title}
                    />
                  );
                }}
              />
            );
          })}
          {_.map(publicRoutes, (route, key) => {
            const { component, path } = route;
            return (
              <Route
                exact
                path={path}
                key={key}
                render={(route) => {
                  return <PublicLayout component={component} route={route} />;
                }}
              />
            );
          })}
          <Route exact path="/" render={() => <Redirect to={"/login"} />} />
        </Switch>
        <NotificationContainer />
      </Router>
    </div>
  );
}

export default App;
