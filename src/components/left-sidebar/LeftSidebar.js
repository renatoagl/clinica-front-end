import React from "react";
import { useHistory } from "react-router-dom";
import cx from "classnames";
import { NavLink as RRNavLink } from "react-router-dom";
import logoBlanco from "../../assets/images/left-sidebar-logo-white.png";
import { NavItem, Nav, NavLink, UncontrolledTooltip } from "reactstrap";
import styles from "./LeftSidebar.module.css";

const LeftSidebar = (props) => {
  const history = useHistory();

  const redirect_to_url = (url) => {
    if (url && url.length > 0) {
      history.push(url);
      history.replace(url);
    }
  };

  return (
    <div id="sidebar-webpage-hidden" className={cx(styles["active"])}>
      <div className={cx(styles["background-image"])}>
        <div className={cx(styles["logo"])}>
          <img src={logoBlanco} />
        </div>
      </div>
      <div className={cx(styles["content-sidebar"])}>
        <Nav
          vertical
          className={cx("list-unstyled", styles["list-unstyled"], "pb-3")}
        >
          <NavItem>
            <NavLink
              tag={RRNavLink}
              to={"/menu-management"}
              className={cx(
                styles["nav-link"],
                history.location.pathname.includes("menu") &&
                  styles["sidebar-nav-link-active"]
              )}
            >
              <i className="ri-table-line"></i>
              <div className={cx(styles["content-text"])}>Tablero</div>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={RRNavLink}
              to={"/patient-management"}
              className={cx(
                styles["nav-link"],
                history.location.pathname.includes("patient") &&
                  styles["sidebar-nav-link-active"]
              )}
            >
              <i className="ri-team-line"></i>
              <div className={cx(styles["content-text"])}>Pacientes</div>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={RRNavLink}
              to={"/diary-management"}
              className={cx(
                styles["nav-link"],
                history.location.pathname.includes("diary") &&
                  styles["sidebar-nav-link-active"]
              )}
            >
              <i className="ri-calendar-check-line"></i>
              <div className={cx(styles["content-text"])}>Agenda</div>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={RRNavLink}
              to={"/reports-management"}
              className={cx(
                styles["nav-link"],
                history.location.pathname.includes("reports") &&
                  styles["sidebar-nav-link-active"]
              )}
            >
              <i className="ri-file-chart-line"></i>
              <div className={cx(styles["content-text"])}>Reportes</div>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={RRNavLink}
              to={"/config-management"}
              className={cx(
                styles["nav-link"],
                history.location.pathname.includes("config") &&
                  styles["sidebar-nav-link-active"]
              )}
            >
              <i className="ri-settings-3-line"></i>
              <div className={cx(styles["content-text"])}>Configuración</div>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={RRNavLink}
              to={"/help-management"}
              className={cx(
                styles["nav-link"],
                history.location.pathname.includes("help") &&
                  styles["sidebar-nav-link-active"]
              )}
            >
              <i className="ri-question-line"></i>
              <div className={cx(styles["content-text"])}>Ayuda</div>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={RRNavLink}
              to={"/login"}
              className={cx(
                styles["nav-link"],
                history.location.pathname.includes("tab") &&
                  styles["sidebar-nav-link-active"]
              )}
            >
              <i className="ri-logout-box-line"></i>
              <div className={cx(styles["content-text"])}>Cerrar Sesión</div>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              tag={RRNavLink}
              to={"/user-management"}
              className={cx(
                styles["nav-link"],
                history.location.pathname.includes("user") &&
                  styles["sidebar-nav-link-active"]
              )}
            >
              <i className="ri-global-line"></i>
              <div className={cx(styles["content-text"])}>Administración</div>
            </NavLink>
          </NavItem>
        </Nav>
      </div>
      <div
        className={cx(
          styles["sidebar-footer"],
          "d-flex",
          "justify-content-center",
          "align-items-center",
          "flex-column",
          "mt-3"
        )}
      >
        <span className={styles.copyright}>Clínica</span>
        <span className={styles.copyright}>Clínica - RCO</span>
        <span className={styles.copyright}>v.1.0.0</span>
      </div>
    </div>
  );
};

export default LeftSidebar;
