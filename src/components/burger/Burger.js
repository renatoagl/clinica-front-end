import React from "react";
import { bool, func } from "prop-types";
import cx from "classnames";
import styles from "./Burger.module.css";

const Burger = ({ isOpen, setOpen }) => {
  return (
    <a
      aria-label="Toggle menu"
      className={cx(styles.link)}
      aria-expanded={isOpen}
      onClick={() => setOpen()}
    >
      <i className="burger-font ri-menu-fold-line ri-sm"></i>
    </a>
  );
};

export default Burger;
