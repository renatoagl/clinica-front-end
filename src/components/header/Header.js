/**
 * App Header
 */
import React from "react";
import cx from "classnames";
import styles from "./Header.module.css";
import Burger from "../burger/Burger";
import { useSelector, useDispatch } from "../../state";

const Header = (props) => {
  const { navCollapsed } = useSelector("layout");
  const dispatch = useDispatch();

  const onToggleNavCollapsed = async () => {
    const val = !navCollapsed;
    dispatch({ type: "COLLAPSED_SIDEBAR", payload: val });
  };

  return (
    <nav className={cx(styles.nav)}>
      <Burger isOpen={!navCollapsed} setOpen={onToggleNavCollapsed} />
      <div className={styles.navRight}>
        <p>Admin</p>
        <i className="ri-shield-user-line ri-2x"></i>
      </div>
    </nav>
  );
};

export default Header;
