/**
 * Rct Collapsible Card
 */
 import React, { Component, Fragment } from 'react';
 import { Collapse, Badge } from 'reactstrap';
 import classnames from 'classnames';
 import { useIntl } from "react-intl"; 
 import FormControl from '@material-ui/core/FormControl';
 import { Input } from 'reactstrap';
 
 // rct section loader
 import RctSectionLoader from '../rct-table-content-loader/RctTableContentLoader';
 
 const RctCollapsibleCard = (props) => {
 
    const { formatMessage: f } = useIntl();

    const state = {
         reload: false,
         collapse: true,
         close: false
    }
 
     const onCollapse = () => {
         this.setState({ collapse: !this.state.collapse });
     }
 
     const onReload = () => {
         this.setState({ reload: true });
         let self = this;
         setTimeout(() => {
             self.setState({ reload: false });
         }, 1500);
     }
 
     const onCloseSection = () => {
         this.setState({ close: true });
     }
 
     return (
             <div className={classnames(props.colClasses ? props.colClasses : '', { 'd-block': !props.collapse })}>
                 <div className={classnames(`rct-block ${props.customClasses ? props.customClasses : ''}`, { 'd-none': state.close })}>
                     {props.heading &&
                         <div className={`rct-block-title ${props.headingCustomClasses ? props.headingCustomClasses : ''}`}>
                             <h4>{props.heading} {props.badge && <Badge className="p-1 ml-10" color={props.badge.class}>{props.badge.name}</Badge>}</h4>
                             {(props.collapsible || props.reloadable || props.closeable || props.editable || props.delible || props.goToDetail || props.withFilter) &&
                                 <div className={props.withFilter ? "contextual-link collapsible-card-filter" : "contextual-link"}>
                                     {props.collapsible && <a href="#" onClick={() => {this.onCollapse();}}><i className="ti-minus"></i></a>}
                                     {props.reloadable &&
                                       <a href="#" onClick={props.onReloadCustomised ? () => {props.onReloadCustomised();} : () => {this.onReload();}}>
                                               <i className="ti-reload" aria-hidden="true" title={f({ id: "components.actions.reload" })}></i>
                                       </a>
                                     }
                                     {props.closeable && <a href="#" onClick={() => {this.onCloseSection();}}><i className="ti-close"></i></a>}
                                     {props.editable &&
                                       <a href="#" onClick={() => {props.onEdit();}}>
                                             <i className="ti-pencil" aria-hidden="true" title={f({ id: "components.actions.edit" })}></i>
                                       </a>
                                     }
                                     {props.goToDetail &&
                                       <a href="#" onClick={() => {props.onEdit();}}>
                                             <i className="ti-more-alt" aria-hidden="true" title={f({ id: "components.actions.goToDetail" })}></i>
                                       </a>
                                     }
                                     {props.delible &&
                                       <a href="#" onClick={() => {props.onDelete();}}>
                                             <i className="ti-close" aria-hidden="true" title={f({ id: "components.actions.delete" })}></i>
                                       </a>
                                     }
                                     {props.withFilter &&
                                       <FormControl fullWidth>
                                         <Input
                                           autoComplete="new-password"
                                           type="select"
                                           className={props.selectedFilter ? "" : "empty-select"}
                                           name="selectedFilter"
                                           id="selectedFilter"
                                           value={props.selectedFilter}
                                           onChange={props.updateFilter}
                                           >
                                                 <option key="" value="">{props.placeholderMessage}</option>
                                           {props.filterValues && props.filterValues.map((item,key) => (
                                             <option key={key} value={item._id}>{item.name}</option>
                                           ))}
                                        </Input>
                                       </FormControl>
                                     }
                                 </div>
                             }
 
                         </div>
                     }
                     <Collapse isOpen={state.collapse}>
                         <div className={classnames(props.contentCustomClasses ? props.contentCustomClasses : '', { "rct-block-content": !props.fullBlock, 'rct-full-block': props.fullBlock })}>
                             {props.children}
                         </div>
                     </Collapse>
                     {(state.reload || props.reloadingCardContent) && <RctSectionLoader />}
                 </div>
             </div>
    );

}
 
 
 export default RctCollapsibleCard;
 