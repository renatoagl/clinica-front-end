import React, { useState } from 'react';

import { Collapse, Card, CardBody } from "reactstrap";
import { useIntl } from "react-intl";
import RefreshAtEnd from '../infinite-scroll/RefreshAtEnd';
import cx from 'classnames';
import styles from './CollapsableList.module.css';
import { useDispatch } from '../../state';

const CollapsableList = (props) => {

    const { formatMessage: f } = useIntl();
    const [openedItem, setOpenedItem] = useState(undefined);
    const dispatch = useDispatch();

    const setNewPage = () => {
		dispatch({ type: 'SET_STOCKS_PAGE' });
	}

    
    return (
        (props.items && props.items.length > 0) ?
            (props.items.map((item, index) =>
                <div
                    key={index} 
                    className={cx(styles['card'])} 
                    selected={true} 
                    onClick={() => props.isCollapsable && openedItem === item._id ? setOpenedItem(undefined): setOpenedItem(item._id)} >
                    <div className={cx(styles.listItemHeader, props.isCollapsable && styles.clickable)}>
                        <div className={styles.textWithLeftIcon}>
                            <div className={styles.cardTitle}>
                                {props.primaryText && <span className={styles.primaryText}>{props.primaryText(item)}</span>}
                                {props.statusText && 
                                    <span className={cx(styles.secondaryText, props.statusTextStyle ? styles[props.statusTextStyle(item)] : styles.secondaryTextColor)}>
                                    {props.statusText(item)}
                                    </span>
                                }
                                {props.secondaryText && 
                                    <span className={cx(styles.secondaryText, props.secondaryTextStyle ? styles[props.secondaryTextStyle(item)] : styles.secondaryTextColor)}>
                                        {props.secondaryText(item)}
                                    </span>
                                }
                            </div>
                        </div>                        
                        <div>
                            {props.isCollapsable &&
                                <i className={cx(styles.iconSize, styles['icon-button'], "ri-file-edit-line", "mr-10")} onClick={() => props.onEditClick(item)}></i>
                            }
                        </div>
                    </div>
                    <Collapse isOpen={props.isCollapsable && openedItem === item._id} className={cx(styles['cardContainer'])}>
                        <hr className={cx(styles['white'])}/>
                        <Card className={cx(styles['cardBody'])}>
                            <CardBody className={cx(styles['card-body'])}>
                                {props.buildCardContent && props.buildCardContent(item)}
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
            ))
            :
                <React.Fragment/>
    )
}


export default CollapsableList;