import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const RctTableContentLoader = () => (
    <tr>
      <td>
        <div className="d-flex justify-content-center progress-primary loader-overlay ">
          <CircularProgress />
        </div>
      </td>
    </tr>
);

export default RctTableContentLoader;