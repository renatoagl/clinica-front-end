import React, { useEffect, useState, useRef } from 'react';


const RefreshAtEnd = (props) => {

	// add loader reference
	const loader = useRef(null);
  const needMoreData = props.needMoreData;
  // here we handle what happens when user scrolls to Load More div
	// in this case we just update page variable
	const handleObserver = (entities) => {
		const target = entities[0];
		if (target.isIntersecting) {
      props.onVisible();
		}
	}

  useEffect(() => {
    let options = {
      root: document.querySelector(`${props.root}`),
      rootMargin: props.rootMargin || '0px',
      threshold: 1.0
    };

    // initialize IntersectionObserver
    // and attaching to Load More div
    const observer = new IntersectionObserver(handleObserver, options);
    if (loader.current) {
      observer.observe(loader.current)
    }
  }, []);

/*  if (needMoreData) {
    return (
      <div className="loading" ref={loader}>
          <h2>Load More</h2>
      </div>
    );
  }
  else {
    return (
      <div></div>
    );
  } */
  return (
    <div className="loading" ref={loader}>
        {/* <h2>Load More</h2> */}
    </div>
  );
}

export default RefreshAtEnd;
