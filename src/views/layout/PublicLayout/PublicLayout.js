import React from "react";

const PublicLayout = (props) => {
  const Component = props.component;
  const route = props.route;

  return (
    <>
      <Component route={route} />
    </>
  );
};

export default PublicLayout;
