import React from "react";
import { useSelector } from "state";
import Header from "components/header/Header";
import Sidebar from "react-sidebar";
import NavBar from "./NavBar";
import LeftSidebar from "components/left-sidebar/LeftSidebar";
import cx from "classnames";
import styles from "./PrivateLayout.module.scss";

const PrivateLayout = (props) => {
  const Component = props.component;
  const title = props.title;
  const route = props.route;
  const { navCollapsed } = useSelector("layout");

  return (
    <div>
      <Sidebar
        rootClassName={styles.Main}
        sidebarClassName={styles.sidebar}
        contentClassName={styles.content}
        sidebar={
          <div>
            <LeftSidebar route={route} />
          </div>
        }
        transitions={true}
        docked={!navCollapsed ? true : false}
      >
        <Header agencyMenu={false} />
        <div className={cx(styles["users-page"])}>
          <NavBar title={title} />
          <Component route={route} />
        </div>
      </Sidebar>
    </div>
  );
};

export default PrivateLayout;
