import React from "react";
import cx from "classnames";
import { useHistory } from "react-router-dom";
import { Button } from "reactstrap";
import styles from "./NavBar.module.scss";
import leftImg from "../../../assets/images/left_1.png";
import { useIntl } from "react-intl";

const NavBar = (props) => {
  const history = useHistory();
  const { formatMessage: f } = useIntl();

  var objectLinks = {
    a: "/member-management",
    b: "/patient-management",
    c: "/appointment-management",
    d: "/config-management/medical-records",
    e: "/config-management/medicines",
    f: "/user-management",
  };
  let showAddButton = false;

  if (
    Object.values(objectLinks).indexOf(history.location.pathname.toString()) >
    -1
  ) {
    showAddButton = true;
  }

  let endLink = history.location.pathname.substring(
    history.location.pathname.lastIndexOf("/") + 1
  );
  let canGoBack = !endLink.includes("management");

  const goBack = () => {
    let endLink = history.location.pathname.substring(
      history.location.pathname.lastIndexOf("/") + 1
    );
    let middleLink = history.location.pathname.substring(
      -1,
      history.location.pathname.lastIndexOf("/")
    );

    if (!endLink.includes("management")) {
      if (middleLink.trim() === "") {
        endLink = endLink.replace("add", "management");
        history.push(endLink);
      } else {
        history.push(
          middleLink.includes("edit") || middleLink.includes("add")
            ? middleLink.replace("edit", "management")
            : middleLink
        );
      }
    }
  };

  const goToAdd = () => {
    let currentUrl = history.location.pathname;
    /* console.log(currentUrl.split("/")); */
    if (currentUrl.split("/").length > 2) {
      currentUrl = currentUrl.concat("-add");
    } else {
      currentUrl = currentUrl.replace("management", "add");
    }
    history.push(currentUrl);
  };

  return (
    <div className={cx(styles["padding"])}>
      <div className={cx(styles["NavBar"])}>
        <div className={cx(styles["flex-title"])}>
          {canGoBack && (
            <a onClick={goBack} className={cx(styles["left-select"])}>
              <img src={leftImg} />
            </a>
          )}
          <h3>{props.title}</h3>
        </div>
        {showAddButton && (
          <div className={cx(styles["contenedorBotonNavBar"])}>
            <Button
              variant="primary"
              className={cx(styles["custom-button"])}
              onClick={goToAdd}
            >
              {f({ id: "app.button.add" })}
            </Button>
          </div>
        )}
      </div>
      <hr />
    </div>
  );
};

export default NavBar;
