import React, { useEffect } from "react";
import { useSelector, useDispatch } from "state";
import { useHistory } from "react-router-dom";
import PatientForm from "./components/PatientForm";

const PatientAdd = (props) => {
  const { patient, savedPatient } = useSelector("patients");
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    const buildEmptyForm = async () => {
      dispatch({ type: "BUILD_EMPTY_PATIENT" });
    };
    buildEmptyForm();
  }, []);

  useEffect(() => {
    if (savedPatient) {
      dispatch({ type: "RESET_SAVED_PATIENT" });
      history.push(`patient-edit/${patient.dni}`);
    }
  }, [savedPatient]);

  return (
    <div>
      {!patient || !patient.dni ? (
        <PatientForm dni={null} />
      ) : (
        <div> Loading... </div>
      )}
    </div>
  );
};

export default PatientAdd;
