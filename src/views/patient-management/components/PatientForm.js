import React, { useState } from "react";
import { Form, Col, Row } from "reactstrap";
import Select from "react-select";
import cx from "classnames";
import styles from "./PatientForm.module.scss";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "state";
import { useParams } from "react-router-dom";
import { persistPatient } from "state/patient";
import { useHistory } from "react-router-dom";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { validateIdentityDocument } from "utils/validator";

const PatientForm = () => {
  const { patient } = useSelector("patients");
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const [sex, setSex] = useState(patient ? patient.sex : null);
  const [birthDate, setBirthDate] = useState(
    patient && patient.birthDate ? new Date(patient.birthDate) : new Date()
  );

  const sexOptions = [
    { label: "M", value: "M" },
    { label: "F", value: "F" },
  ];

  Yup.addMethod(Yup.string, "nifValidation", function (message) {
    return this.test("nifValidation", message, function (value) {
      const { path, createError } = this;
      const document = value;
      let documentOk = validateIdentityDocument(document);
      if (!documentOk) {
        return createError({ path, message: message });
      } else {
        return true;
      }
    });
  });

  const patientSchema = Yup.object({
    dni: Yup.string()
      .required("NIF/NIE requerido")
      .nifValidation("DNI Formato incorrecto"),
    name: Yup.string().required("Nombre requerido"),
    fileNumber: Yup.string().required("Nº expediente requerido"),
  }).required();

  const { register, handleSubmit, setValue, errors } = useForm({
    mode: "onBlur",
    resolver: yupResolver(patientSchema),
    defaultValues: patient
      ? {
          ...patient,
          sex: patient.sex ? patient.sex : undefined,
        }
      : {
          dni: undefined,
          name: undefined,
          fileNumber: undefined,
          telephone: undefined,
          sex: undefined,
          municipality: undefined,
          province: undefined,
          birthDate: undefined,
        },
  });

  const onChangeSex = (sex) => {
    setSex(sex);
    setValue("sex", sex ? sex.value : "");
  };

  const onSubmit = async (data) => {
    data.birthDate = birthDate;
    data.sex = sex.value;
    console.log(data);
    await persistPatient(dispatch, data, id);
  };

  return (
    <div className={cx(styles["form-content"])}>
      <Form id="patient-form" onSubmit={handleSubmit(onSubmit)}>
        <input ref={register} type="hidden" id="sex" name="sex" />

        <div className={cx(styles["form-content"])}>
          <Row className={cx(styles["form-content-row"])}>
            {history.location.pathname === "/patient-add" ? (
              <>
                <Col xs="2">
                  <label>NIF/NIE</label>
                </Col>
                <Col xs="2">
                  <input
                    ref={register}
                    type="text"
                    name="dni"
                    id="dni"
                    className={cx(styles["input-form"])}
                  />
                  {errors.dni && (
                    <>
                      <br></br>
                      <span className={cx(styles["error"])}>
                        {errors.dni.message}
                      </span>
                    </>
                  )}
                </Col>
              </>
            ) : (
              <>
                <Col xs="2">
                  <label>NIF/NIE</label>
                </Col>
                <Col xs="2">
                  <input
                    ref={register}
                    type="text"
                    name="dni"
                    id="dni"
                    className={cx(styles["input-form"])}
                    /* disabled */
                  />
                  {errors.dni && (
                    <>
                      <br></br>
                      <span className={cx(styles["error"])}>
                        {errors.dni.message}
                      </span>
                    </>
                  )}
                </Col>
              </>
            )}
            <Col xs="2">
              <label>Nombre</label>
            </Col>
            <Col xs="2">
              <input
                ref={register}
                type="text"
                name="name"
                id="name"
                className={cx(styles["input-form"])}
              />
            </Col>
          </Row>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Nº Expediente</label>
            </Col>
            <Col xs="2">
              <input
                ref={register}
                type="text"
                name="fileNumber"
                id="fileNumber"
                className={cx(styles["input-form"])}
              />
            </Col>
            <Col xs="2">
              <label>Teléfono</label>
            </Col>
            <Col xs="2">
              <input
                ref={register}
                type="text"
                name="telephone"
                id="telephone"
                className={cx(styles["input-form"])}
              />
            </Col>
          </Row>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Municipio</label>
            </Col>
            <Col xs="2">
              <input
                ref={register}
                type="text"
                name="municipality"
                id="municipality"
                className={cx(styles["input-form"])}
              />
            </Col>
            <Col xs="2">
              <label>Provincia</label>
            </Col>
            <Col xs="2">
              <input
                ref={register}
                type="text"
                name="province"
                id="province"
                className={cx(styles["input-form"])}
              />
            </Col>
          </Row>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Sexo</label>
            </Col>
            <Col xs="2">
              <Select
                options={sexOptions}
                value={sex}
                isClearable={true}
                placeholder=""
                onChange={(sex) => {
                  onChangeSex(sex);
                }}
              />
            </Col>
            <Col xs="2">
              <label>Fecha de nacimiento</label>
            </Col>
            <Col xs="2">
              <DatePicker
                selected={birthDate}
                onChange={(e) => {
                  setBirthDate(e);
                }}
                className={cx(styles["input-form"])}
              />
            </Col>
          </Row>
          <Row>
            <Col sm={{ size: 2, offset: 10 }}>
              <div className={cx(styles["main-form-button"])}>
                <button
                  form="patient-form"
                  className={cx(styles["custom-button"])}
                  type="submit"
                >
                  Guardar
                </button>
              </div>
            </Col>
          </Row>
        </div>
      </Form>
    </div>
  );
};

export default PatientForm;
