import React, { useEffect, useState, useRef } from "react";
import cx from "classnames";
import styles from "./PatientFilter.module.scss";
import { getAllPatients, getPatientsFiltered } from "state/patient";
import { useSelector, useDispatch } from "state";
import { Col, Row, FormGroup, Input, Button } from "reactstrap";

const PatientFilter = () => {
  const { searchTextFilter } = useSelector("patients");
  const dispatch = useDispatch();

  const onSearchTextChange = async (searchText) => {
    dispatch({ type: "SEARCH_TEXT_CHANGE", payload: searchText });
  };

  const onSubmit = async () => {
    if (searchTextFilter === "") {
      await getAllPatients(dispatch);
    } else {
      const paramsObject = { text: searchTextFilter };
      await getPatientsFiltered(dispatch, paramsObject);
    }
  };

  return (
    <div className={cx(styles["patients-headers"])}>
      <Row>
        <Col xs="9">
          <FormGroup>
            <Input
              className={cx(styles["padding-filter"])}
              value={searchTextFilter}
              onChange={(e) => {
                onSearchTextChange(e.target.value);
              }}
              type="text"
              name="text"
              placeholder="NIF o Nombre"
            />
          </FormGroup>
        </Col>
        <Col xs="3">
          <Button
            className={cx(styles["padding-search-button"])}
            onClick={onSubmit}
          >
            BUSCAR
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default PatientFilter;
