import React, { useEffect, Fragment } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "state";
import { getAllPatients } from "state/patient";
import { Table } from "reactstrap";
import PatientFilter from "./PatientFilter";
import "./PatientList.scss";

const PatientList = (props) => {
  const { patients } = useSelector("patients");
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const getPatientsFunction = async () => {
      await getAllPatients(dispatch);
    };
    getPatientsFunction();
  }, []);

  const goToEditMode = async (patient) => {
    history.push("patient-edit/" + patient.dni);
  };

  return (
    <>
      <PatientFilter />
      <div className="tableFixHead">
        <Table hover responsive>
          <thead>
            <tr>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color">DNI</th>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                Nombre y apellidos
              </th>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                Nº Expediente
              </th>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color">Sexo</th>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color"></th>
            </tr>
          </thead>
          <tbody>
            <Fragment>
              {patients &&
                patients.length > 0 &&
                patients.map((patient, key) => {
                  return (
                    <tr key={key}>
                      <td className="gray-text">
                        {patient.dni ? patient.dni : "-"}
                      </td>
                      <td>{patient.name ? patient.name : "-"}</td>
                      <td className="gray-text">
                        {patient.fileNumber ? patient.fileNumber : "-"}
                      </td>
                      <td className="gray-text">
                        {patient.sex ? patient.sex : "-"}
                      </td>
                      <td
                        onClick={() => {
                          goToEditMode(patient);
                        }}
                      >
                        <i className="ri-edit-2-line"></i>
                      </td>
                    </tr>
                  );
                })}
            </Fragment>
          </tbody>
        </Table>
      </div>
    </>
  );
};

export default PatientList;
