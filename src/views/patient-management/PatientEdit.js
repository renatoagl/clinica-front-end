import React, { useEffect } from "react";
import { getPatient } from "state/patient";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "state";
import PatientForm from "./components/PatientForm";

const PatientEdit = (props) => {
  const { patient } = useSelector("patients");
  const dispatch = useDispatch();
  const { id } = useParams();

  useEffect(() => {
    const getPatientData = async () => {
      await getPatient(dispatch, id);
    };
    getPatientData();
  }, []);

  return (
    <div>{patient ? <PatientForm dni={id} /> : <div> Loading... </div>}</div>
  );
};

export default PatientEdit;
