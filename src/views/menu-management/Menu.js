import React, { useEffect, Fragment } from "react";
import cx from "classnames";
import styles from "./Menu.module.scss";
import { Table } from "reactstrap";
import { useSelector, useDispatch } from "state";
import {
  countAppointmentsPerDoctor,
  countConsultationsPerDoctor,
  countPatientsPerDoctor,
  getMenuPatients,
} from "state/menu";

const Menu = () => {
  const { numberAppointments, numberConsultations, numberPatients, patients } =
    useSelector("menu");
  const dispatch = useDispatch();

  useEffect(() => {
    const getInfo = async () => {
      await countAppointmentsPerDoctor(dispatch);
      await countConsultationsPerDoctor(dispatch);
      await countPatientsPerDoctor(dispatch);
      await getMenuPatients(dispatch);
    };
    getInfo();
  }, []);

  return (
    <div className={cx(styles["menu-container"])}>
      <div className={cx(styles["boxes-container"])}>
        <div className={cx(styles["blue-box"])}>
          <div className={cx(styles["top-box"])}>
            <i className={cx(styles["icon"], "ri-6x ri-book-2-fill")}></i>
            <br></br>
            {numberAppointments === 1 ? (
              <span>{numberAppointments} Cita</span>
            ) : (
              <span>{numberAppointments} Citas</span>
            )}
          </div>
          <div className={cx(styles["bottom-box"])}>
            Cantidad de citas restantes a realizar hoy
          </div>
        </div>
        <div className={cx(styles["green-box"])}>
          <div className={cx(styles["top-box"])}>
            <i className={cx(styles["icon"], "ri-6x ri-stethoscope-line")}></i>
            <br></br>
            {numberConsultations === 1 ? (
              <span>{numberConsultations} Consulta</span>
            ) : (
              <span>{numberConsultations} Consultas</span>
            )}
          </div>
          <div className={cx(styles["bottom-box"])}>
            Cantidad de consultas que se realizaron hoy
          </div>
        </div>
        <div className={cx(styles["red-box"])}>
          <div className={cx(styles["top-box"])}>
            <i className={cx(styles["icon"], "ri-6x ri-group-2-fill")}></i>
            <br></br>
            {numberPatients === 1 ? (
              <span>{numberPatients} Paciente</span>
            ) : (
              <span>{numberPatients} Pacientes</span>
            )}
          </div>
          <div className={cx(styles["bottom-box"])}>
            Cantidad de pacientes en el sistema
          </div>
        </div>
      </div>
      <div className={cx(styles["table-container"])}>
        <p>Pacientes citados de hoy</p>
        <div className={cx(styles["menu-table-container"])}>
          <Table hover responsive>
            <thead>
              <tr>
                <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                  Paciente
                </th>
                <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                  Nº Expediente
                </th>
                <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                  Urgencia
                </th>
                <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                  Fecha Programada
                </th>
                <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                  Hora Programada
                </th>
                <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                  Nº Orden
                </th>
              </tr>
            </thead>
            <tbody>
              <Fragment>
                {patients &&
                  patients.length > 0 &&
                  patients.map((patient, key) => {
                    return (
                      <tr key={key}>
                        <td className="gray-text">
                          {patient.nombre ? patient.nombre : "-"}
                        </td>
                        <td>
                          {patient.num_expediente
                            ? patient.num_expediente
                            : "-"}
                        </td>
                        <td className="gray-text">
                          {patient.urgencia ? patient.urgencia : "-"}
                        </td>
                        <td className="gray-text">
                          {patient.fecha_programada
                            ? patient.fecha_programada
                            : "-"}
                        </td>
                        <td className="gray-text">
                          {patient.hora_programada
                            ? patient.hora_programada
                            : "-"}
                        </td>
                        <td className="gray-text">
                          {patient.numero_orden ? patient.numero_orden : "-"}
                        </td>
                      </tr>
                    );
                  })}
              </Fragment>
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default Menu;
