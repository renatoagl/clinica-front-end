import React, { useEffect } from "react";
import { useSelector, useDispatch } from "state";
import { useHistory } from "react-router-dom";
import MedicalRecordFilter from "./MedicalRecordFilter";
import cx from "classnames";
import styles from "./MedicalRecordList.module.scss";
import { getAllMedicalRecords } from "state/configuration";

const MedicalRecordList = () => {
  const { medicalRecords, loading } = useSelector("configurations");
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const getMedicalRecordsFunction = async () => {
      await getAllMedicalRecords(dispatch);
    };
    getMedicalRecordsFunction();
  }, []);

  const medicalRecordMap =
    medicalRecords &&
    medicalRecords.map((medicalRecord, key) => (
      <div key={key} className={cx(styles["registry-container-row"])}>
        <div className="registry-container-row-left">{medicalRecord.id}</div>
        <div className="registry-container-row-left">
          <label className={cx(styles["registry-container-title"])}>
            {medicalRecord.name}
          </label>
        </div>

        <div className={cx(styles["registry-container-right"])}>
          <i
            onClick={() => {
              history.push(`medical-records/${medicalRecord.id}`);
            }}
            className={cx(styles["icono"], "ri-pencil-fill")}
          ></i>
        </div>
      </div>
    ));

  return (
    <div className="container-fluid">
      <MedicalRecordFilter />
      {loading ? <span>Loading...</span> : <>{medicalRecordMap}</>}
    </div>
  );
};

export default MedicalRecordList;
