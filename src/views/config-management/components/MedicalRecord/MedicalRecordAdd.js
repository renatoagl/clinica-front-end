import React, { useEffect } from "react";
import { useSelector, useDispatch } from "state";
import { useHistory } from "react-router-dom";
import MedicalRecordForm from "./MedicalRecordForm";
import { getTypes } from "state/configuration";

const MedicalRecordAdd = () => {
  const { medicalRecord, savedMedicalRecord } = useSelector("configurations");
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    const buildEmptyForm = async () => {
      dispatch({ type: "BUILD_EMPTY_MEDICAL_RECORD" });
    };
    buildEmptyForm();
  }, []);

  useEffect(() => {
    if (savedMedicalRecord) {
      dispatch({ type: "RESET_SAVED_MEDICAL_RECORD" });
      history.push(`medical-records/${medicalRecord.id}`);
    }
  }, [savedMedicalRecord]);

  useEffect(() => {
    const getTypesDescriptions = async () => {
      await getTypes(dispatch);
    };
    getTypesDescriptions();
  }, []);

  return (
    <div>
      {!medicalRecord || !medicalRecord.id ? (
        <MedicalRecordForm id={null} />
      ) : (
        <div> Loading... </div>
      )}
    </div>
  );
};

export default MedicalRecordAdd;
