import React, { useEffect } from "react";
import { getMedicalRecord, getTypes } from "state/configuration";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "state";
import MedicalRecordForm from "./MedicalRecordForm";

const MedicalRecordEdit = () => {
  const { medicalRecord } = useSelector("configurations");
  const dispatch = useDispatch();
  const { id } = useParams();

  useEffect(() => {
    const getMedicalRecordData = async () => {
      await getMedicalRecord(dispatch, id);
    };
    getMedicalRecordData();
  }, []);

  useEffect(() => {
    const getTypesDescriptions = async () => {
      await getTypes(dispatch);
    };
    getTypesDescriptions();
  }, []);

  return (
    <div>
      {medicalRecord ? <MedicalRecordForm id={id} /> : <div> Loading... </div>}
    </div>
  );
};

export default MedicalRecordEdit;
