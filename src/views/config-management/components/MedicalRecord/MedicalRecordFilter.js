import React, { useEffect, useState } from "react";
import cx from "classnames";
import styles from "views/admin-management/components/UserFilter.module.scss";
import { useSelector, useDispatch } from "state";
import { Col, Row, FormGroup, Button, Input } from "reactstrap";
import Select from "react-select";
import {
  getAllMedicalRecords,
  getMedicalRecordsFiltered,
  getTypes,
} from "state/configuration";

const MedicalRecordFilter = () => {
  const { searchTextFilter, types, searchType } = useSelector("configurations");
  const dispatch = useDispatch();

  useEffect(() => {
    const getTypesDescriptions = async () => {
      await getTypes(dispatch);
    };
    getTypesDescriptions();
  }, []);

  const onSearchTextChange = async (searchText) => {
    dispatch({ type: "SEARCH_TEXT_CHANGE", payload: searchText });
  };

  const onSearchTypeChange = async (searchType) => {
    dispatch({ type: "SEARCH_TYPE", payload: searchType });
  };

  const onSubmit = async () => {
    if (searchTextFilter === "" && !searchType) {
      await getAllMedicalRecords(dispatch);
    } else {
      const paramsObject = {
        text: searchTextFilter,
        type: searchType ? searchType.value : undefined,
      };
      await getMedicalRecordsFiltered(dispatch, paramsObject);
    }
  };

  return (
    <div className={cx(styles["users-headers"])}>
      <Row>
        <Col xs="6">
          <FormGroup>
            <Input
              className={cx(styles["padding-filter"])}
              value={searchTextFilter}
              onChange={(e) => {
                onSearchTextChange(e.target.value);
              }}
              type="text"
              name="text"
              placeholder="Antecedente"
            />
          </FormGroup>
        </Col>
        <Col xs="3">
          <FormGroup>
            <Select
              options={types}
              value={searchType}
              isClearable={true}
              placeholder={"Seleccionar tipo"}
              onChange={(type) => {
                onSearchTypeChange(type);
              }}
            />
          </FormGroup>
        </Col>
        <Col xs="3">
          <Button
            className={cx(styles["padding-search-button"])}
            onClick={onSubmit}
          >
            BUSCAR
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default MedicalRecordFilter;
