import React, { useState } from "react";
import { Form, Col, Row } from "reactstrap";
import Select from "react-select";
import cx from "classnames";
import styles from "./MedicalRecordForm.module.scss";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "state";
import { useParams } from "react-router-dom";
import { persistMedicalRecord } from "state/configuration";
import { useHistory } from "react-router-dom";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const MedicalRecordForm = () => {
  const { medicalRecord, types } = useSelector("configurations");
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const [type, setType] = useState(medicalRecord ? medicalRecord.type : null);

  const medicalRecordSchema = Yup.object({
    name: Yup.string().required("Nombre requerido"),
    type: Yup.string().required("Tipo requerido"),
  }).required();

  const { register, handleSubmit, setValue, errors } = useForm({
    mode: "onBlur",
    resolver: yupResolver(medicalRecordSchema),
    defaultValues: medicalRecord
      ? {
          ...medicalRecord,
          type: medicalRecord.type ? medicalRecord.type : undefined,
        }
      : {
          name: undefined,
          type: undefined,
        },
  });

  const onChangeType = (type) => {
    setType(type);
    setValue("type", type ? type.value : "");
  };

  const onSubmit = async (data) => {
    console.log(data);
    await persistMedicalRecord(dispatch, data, id);
  };

  return (
    <div className={cx(styles["form-content"])}>
      <Form id="medical-record-form" onSubmit={handleSubmit(onSubmit)}>
        <input ref={register} type="hidden" id="type" name="type" />

        <div className={cx(styles["form-content"])}>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Nombre</label>
            </Col>
            <Col xs="4">
              <input
                ref={register}
                type="text"
                name="name"
                id="name"
                className={cx(styles["input-form"])}
              />
              {errors.name && (
                <>
                  <br></br>
                  <span className={cx(styles["error"])}>
                    {errors.name.message}
                  </span>
                </>
              )}
            </Col>
          </Row>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Especialidad</label>
            </Col>
            <Col xs="4">
              <Select
                options={types}
                value={type}
                isClearable={true}
                placeholder=""
                onChange={(type) => {
                  onChangeType(type);
                }}
              />
              {errors.type && (
                <>
                  <br></br>
                  <span className={cx(styles["error"])}>
                    {errors.type.message}
                  </span>
                </>
              )}
            </Col>
          </Row>
          <Row>
            <Col sm={{ size: 2, offset: 10 }}>
              <div className={cx(styles["main-form-button"])}>
                <button
                  form="medical-record-form"
                  className={cx(styles["custom-button"])}
                  type="submit"
                >
                  Guardar
                </button>
              </div>
            </Col>
          </Row>
        </div>
      </Form>
    </div>
  );
};

export default MedicalRecordForm;
