import React, { useEffect, useState } from "react";
import cx from "classnames";
import styles from "views/admin-management/components/UserFilter.module.scss";
import { useSelector, useDispatch } from "state";
import { Col, Row, FormGroup, Button, Input } from "reactstrap";
import Select from "react-select";
import { getAllMedicines, getMedicinesFiltered } from "state/configuration";

const MedicineFilter = () => {
  const { searchTextFilter } = useSelector("configurations");
  const dispatch = useDispatch();

  const onSearchTextChange = async (searchText) => {
    dispatch({ type: "SEARCH_TEXT_CHANGE", payload: searchText });
  };

  const onSubmit = async () => {
    if (searchTextFilter === "") {
      await getAllMedicines(dispatch);
    } else {
      const paramsObject = {
        text: searchTextFilter,
      };
      await getMedicinesFiltered(dispatch, paramsObject);
    }
  };

  return (
    <div className={cx(styles["users-headers"])}>
      <Row>
        <Col xs="9">
          <FormGroup>
            <Input
              className={cx(styles["padding-filter"])}
              value={searchTextFilter}
              onChange={(e) => {
                onSearchTextChange(e.target.value);
              }}
              type="text"
              name="text"
              placeholder="Medicamento"
            />
          </FormGroup>
        </Col>
        <Col xs="3">
          <Button
            className={cx(styles["padding-search-button"])}
            onClick={onSubmit}
          >
            BUSCAR
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default MedicineFilter;
