import React from "react";
import { Form, Col, Row } from "reactstrap";
import cx from "classnames";
import styles from "./MedicineForm.module.scss";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "state";
import { useParams } from "react-router-dom";
import { persistMedicine } from "state/configuration";
import { useHistory } from "react-router-dom";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const MedicineForm = () => {
  const { medicine } = useSelector("configurations");
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const medicineSchema = Yup.object({
    name: Yup.string().required("Nombre requerido"),
  }).required();

  const { register, handleSubmit, setValue, errors } = useForm({
    mode: "onBlur",
    resolver: yupResolver(medicineSchema),
    defaultValues: medicine
      ? {
          ...medicine,
        }
      : {
          name: undefined,
        },
  });

  const onSubmit = async (data) => {
    console.log(data);
    await persistMedicine(dispatch, data, id);
  };

  return (
    <div className={cx(styles["form-content"])}>
      <Form id="medicine-form" onSubmit={handleSubmit(onSubmit)}>
        <input ref={register} type="hidden" id="type" name="type" />

        <div className={cx(styles["form-content"])}>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Nombre</label>
            </Col>
            <Col xs="4">
              <input
                ref={register}
                type="text"
                name="name"
                id="name"
                className={cx(styles["input-form"])}
              />
              {errors.name && (
                <>
                  <br></br>
                  <span className={cx(styles["error"])}>
                    {errors.name.message}
                  </span>
                </>
              )}
            </Col>
          </Row>
          <Row>
            <Col sm={{ size: 2, offset: 10 }}>
              <div className={cx(styles["main-form-button"])}>
                <button
                  form="medicine-form"
                  className={cx(styles["custom-button"])}
                  type="submit"
                >
                  Guardar
                </button>
              </div>
            </Col>
          </Row>
        </div>
      </Form>
    </div>
  );
};

export default MedicineForm;
