import React, { useEffect } from "react";
import { useSelector, useDispatch } from "state";
import { useHistory } from "react-router-dom";
import MedicineForm from "./MedicineForm";
import { getTypes } from "state/configuration";

const MedicineAdd = () => {
  const { medicine, savedMedicine } = useSelector("configurations");
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    const buildEmptyForm = async () => {
      dispatch({ type: "BUILD_EMPTY_MEDICINE" });
    };
    buildEmptyForm();
  }, []);

  useEffect(() => {
    if (savedMedicine) {
      dispatch({ type: "RESET_SAVED_MEDICINE" });
      history.push(`medicines/${medicine.id}`);
    }
  }, [savedMedicine]);

  return (
    <div>
      {!medicine || !medicine.id ? (
        <MedicineForm id={null} />
      ) : (
        <div> Loading... </div>
      )}
    </div>
  );
};

export default MedicineAdd;
