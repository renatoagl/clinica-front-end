import React, { useEffect } from "react";
import { useSelector, useDispatch } from "state";
import { useHistory } from "react-router-dom";
import MedicineFilter from "./MedicineFilter";
import cx from "classnames";
import styles from "./MedicineList.module.scss";
import { getAllMedicines } from "state/configuration";

const MedicineList = () => {
  const { medicines, loading } = useSelector("configurations");
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const getMedicinesFunction = async () => {
      await getAllMedicines(dispatch);
    };
    getMedicinesFunction();
  }, []);

  const medicalRecordMap =
    medicines &&
    medicines.map((medicine, key) => (
      <div key={key} className={cx(styles["registry-container-row"])}>
        <div className="registry-container-row-left">{medicine.id}</div>
        <div className="registry-container-row-left">
          <label className={cx(styles["registry-container-title"])}>
            {medicine.name}
          </label>
        </div>
        <div className={cx(styles["registry-container-right"])}>
          <i
            onClick={() => {
              history.push(`medicines/${medicine.id}`);
            }}
            className={cx(styles["icono"], "ri-pencil-fill")}
          ></i>
        </div>
      </div>
    ));

  return (
    <div className="container-fluid">
      <MedicineFilter />
      {loading ? <span>Loading...</span> : <>{medicalRecordMap}</>}
    </div>
  );
};

export default MedicineList;
