import React, { useEffect } from "react";
import { getMedicine } from "state/configuration";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "state";
import MedicineForm from "./MedicineForm";

const MedicineEdit = () => {
  const { medicine } = useSelector("configurations");
  const dispatch = useDispatch();
  const { id } = useParams();

  useEffect(() => {
    const getMedicineData = async () => {
      await getMedicine(dispatch, id);
    };
    getMedicineData();
  }, []);

  return (
    <div>{medicine ? <MedicineForm id={id} /> : <div> Loading... </div>}</div>
  );
};

export default MedicineEdit;
