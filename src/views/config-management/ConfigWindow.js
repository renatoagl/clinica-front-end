import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "state";
import { Row, Col } from "reactstrap";
import cx from "classnames";
import styles from "./ConfigWindow.module.scss";
import { getAllMedicalRecords, getAllMedicines } from "state/configuration";

const ConfigWindow = () => {
  let history = useHistory();
  const dispatch = useDispatch();

  const optionsAux = [
    {
      title: "Antecedentes Médicos",
      redirectTo: "config-management/medical-records",
    },
    { title: "Medicamentos", redirectTo: "config-management/medicines" },
    { title: "Agenda", redirectTo: "config-management/diary-edit/:id" },
  ];

  useEffect(() => {
    const getMedicalRecordsFunction = async () => {
      await getAllMedicalRecords(dispatch);
    };
    getMedicalRecordsFunction();
  }, []);

  useEffect(() => {
    const getMedicinesFunction = async () => {
      await getAllMedicines(dispatch);
    };
    getMedicinesFunction();
  }, []);

  const redirectToLink = async (link) => {
    history.push(link);
  };

  const optionsMapped = optionsAux.map((optionAux, key) => (
    <Col
      xs="3"
      className={cx(styles["card-tab-management"])}
      key={key}
      onClick={() => {
        redirectToLink(optionAux.redirectTo);
      }}
    >
      {optionAux.title}
    </Col>
  ));

  return (
    <div className={cx(styles["container-fluid"])}>
      <div className={cx(styles["title"])}>
        Seleccione la sección que desee configurar:
      </div>
      <Row className={cx(styles["gap-card-container"])}>{optionsMapped}</Row>
    </div>
  );
};

export default ConfigWindow;
