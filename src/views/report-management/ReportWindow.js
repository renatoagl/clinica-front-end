import React from "react";
import { Row, Col } from "reactstrap";
import cx from "classnames";
import styles from "./ReportWindow.module.scss";
import { post, get, put, deleteMethod } from "../../api/client";
import { NotificationManager } from "react-notifications";

const ReportWindow = () => {
  const printMedicines = async () => {
    try {
      await get(`reports/download?document=Medicines`);
      NotificationManager.success("Listado de medicamentos descargado.");
    } catch (error) {
      console.log(error);
    }
  };

  const printPatients = async () => {
    try {
      await get(`reports/download?document=Patients`);
      NotificationManager.success("Listado de pacientes descargado.");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className={cx(styles["container-fluid"])}>
      <div className={cx(styles["title"])}>
        Seleccione el documento a descargar:
      </div>
      <Row className={cx(styles["gap-card-container"])}>
        <Col
          xs="3"
          className={cx(styles["report-button"])}
          onClick={() => {
            printMedicines();
          }}
        >
          Listado de medicamentos
        </Col>
        <Col
          xs="3"
          className={cx(styles["report-button"])}
          onClick={() => {
            printPatients();
          }}
        >
          Listado de pacientes
        </Col>
      </Row>
    </div>
  );
};

export default ReportWindow;
