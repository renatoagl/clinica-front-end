import React, { useEffect, Fragment } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "state";
import { getAllUsers } from "state/admin";
import { Table } from "reactstrap";
import UserFilter from "./UserFilter";
import "./UserList.scss";

const UserList = () => {
  const { users } = useSelector("users");
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const getUsersFunction = async () => {
      await getAllUsers(dispatch);
    };
    getUsersFunction();
  }, []);

  const goToEditMode = async (user) => {
    history.push("user-edit/" + user.id);
  };

  return (
    <>
      <UserFilter />
      <div className="tableFixHead">
        <Table hover responsive>
          <thead>
            <tr>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                Usuario
              </th>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                Nombre y apellidos
              </th>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                Correo Electrónico
              </th>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color">
                Especialidad
              </th>
              <th className="col-sm-2 col-md-2 col-lg-2 title-color"></th>
            </tr>
          </thead>
          <tbody>
            <Fragment>
              {users &&
                users.length > 0 &&
                users.map((user, key) => {
                  return (
                    <tr key={key}>
                      <td className="gray-text">
                        {user.user ? user.user : "-"}
                      </td>
                      <td>{user.name ? user.name : "-"}</td>
                      <td className="gray-text">
                        {user.email ? user.email : "-"}
                      </td>
                      <td className="gray-text">
                        {user.speciality ? user.speciality : "-"}
                      </td>
                      <td
                        onClick={() => {
                          goToEditMode(user);
                        }}
                      >
                        <i className="ri-edit-2-line"></i>
                      </td>
                    </tr>
                  );
                })}
            </Fragment>
          </tbody>
        </Table>
      </div>
    </>
  );
};

export default UserList;
