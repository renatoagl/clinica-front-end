import React, { useState } from "react";
import { Form, Col, Row } from "reactstrap";
import Select from "react-select";
import cx from "classnames";
import styles from "./UserForm.module.scss";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "state";
import { useParams } from "react-router-dom";
import { persistUser } from "state/admin";
import { useHistory } from "react-router-dom";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const UserForm = () => {
  const { user, specialities } = useSelector("users");
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const [sex, setSex] = useState(user ? user.sex : null);
  const [speciality, setSpeciality] = useState(user ? user.speciality : null);

  const sexOptions = [
    { label: "M", value: "M" },
    { label: "F", value: "F" },
  ];

  const userSchema = Yup.object({
    user: Yup.string().required("Nombre de usuario requerido"),
    name: Yup.string().required("Nombre requerido"),
    password: Yup.string().required("Contraseña requerida"),
    email: Yup.string().email("Formato de correo incorrecto"),
  }).required();

  const { register, handleSubmit, setValue, errors } = useForm({
    mode: "onBlur",
    resolver: yupResolver(userSchema),
    defaultValues: user
      ? {
          ...user,
          speciality: user.speciality ? user.speciality : undefined,
          sex: user.sex ? user.sex : undefined,
        }
      : {
          name: undefined,
          user: undefined,
          email: undefined,
          speciality: undefined,
          sex: undefined,
          password: undefined,
        },
  });

  const onChangeSpeciality = (speciality) => {
    setSpeciality(speciality);
    setValue("speciality", speciality ? speciality.value : "");
  };

  const onChangeSex = (sex) => {
    setSex(sex);
    setValue("sex", sex ? sex.value : "");
  };

  const onSubmit = async (data) => {
    data.sex = sex.value;
    data.speciality = speciality.value;
    await persistUser(dispatch, data, id);
  };

  const createPassword = () => {
    var pass = "";
    var str =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz0123456789@#$";

    for (let i = 1; i <= 8; i++) {
      var char = Math.floor(Math.random() * str.length + 1);

      pass += str.charAt(char);
    }

    setValue("password", pass);
  };

  return (
    <div className={cx(styles["form-content"])}>
      <Form id="user-form" onSubmit={handleSubmit(onSubmit)}>
        <input ref={register} type="hidden" id="speciality" name="speciality" />
        <input ref={register} type="hidden" id="sex" name="sex" />

        <div className={cx(styles["form-content"])}>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Nombre</label>
            </Col>
            <Col xs="4">
              <input
                ref={register}
                type="text"
                name="name"
                id="name"
                className={cx(styles["input-form"])}
              />
              {errors.name && (
                <>
                  <br></br>
                  <span className={cx(styles["error"])}>
                    {errors.name.message}
                  </span>
                </>
              )}
            </Col>
          </Row>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Usuario</label>
            </Col>
            <Col xs="4">
              <input
                ref={register}
                type="text"
                name="user"
                id="user"
                className={cx(styles["input-form"])}
              />
              {errors.user && (
                <>
                  <br></br>
                  <span className={cx(styles["error"])}>
                    {errors.user.message}
                  </span>
                </>
              )}
            </Col>
          </Row>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Correo electrónico</label>
            </Col>
            <Col xs="4">
              <input
                ref={register}
                type="text"
                name="email"
                id="email"
                className={cx(styles["input-form"])}
              />
              {errors.email && (
                <>
                  <br></br>
                  <span className={cx(styles["error"])}>
                    {errors.email.message}
                  </span>
                </>
              )}
            </Col>
          </Row>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Especialidad</label>
            </Col>
            <Col xs="4">
              <Select
                options={specialities}
                value={speciality}
                isClearable={true}
                placeholder=""
                onChange={(speciality) => {
                  onChangeSpeciality(speciality);
                }}
              />
            </Col>
          </Row>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Sexo</label>
            </Col>
            <Col xs="4">
              <Select
                options={sexOptions}
                value={sex}
                isClearable={true}
                placeholder=""
                onChange={(sex) => {
                  onChangeSex(sex);
                }}
              />
            </Col>
          </Row>
          <Row className={cx(styles["form-content-row"])}>
            <Col xs="2">
              <label>Contraseña</label>
            </Col>
            <Col xs="4">
              {history.location.pathname === "/user-add" ? (
                <>
                  <input
                    ref={register}
                    type="hidden"
                    name="password"
                    id="password"
                  />
                  <button
                    type="button"
                    onClick={createPassword}
                    className={cx(styles["generate-password-button"])}
                  >
                    Generar
                  </button>
                </>
              ) : (
                <input
                  ref={register}
                  type="password"
                  name="password"
                  id="password"
                  className={cx(styles["input-form"])}
                />
              )}
              {errors.password && (
                <>
                  <br></br>
                  <span className={cx(styles["error"])}>
                    {errors.password.message}
                  </span>
                </>
              )}
            </Col>
          </Row>
          <Row>
            <Col sm={{ size: 2, offset: 10 }}>
              <div className={cx(styles["main-form-button"])}>
                <button
                  form="user-form"
                  className={cx(styles["custom-button"])}
                  type="submit"
                >
                  Guardar
                </button>
              </div>
            </Col>
          </Row>
        </div>
      </Form>
    </div>
  );
};

export default UserForm;
