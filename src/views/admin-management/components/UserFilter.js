import React, { useEffect, useState, useRef } from "react";
import cx from "classnames";
import styles from "./UserFilter.module.scss";
import { getAllUsers, getUsersFiltered } from "state/admin";
import { useSelector, useDispatch } from "state";
import { Col, Row, FormGroup, Input, Button } from "reactstrap";

const UserFilter = () => {
  const { searchTextFilter } = useSelector("users");
  const dispatch = useDispatch();

  const onSearchTextChange = async (searchText) => {
    dispatch({ type: "SEARCH_TEXT_CHANGE", payload: searchText });
  };

  const onSubmit = async () => {
    if (searchTextFilter === "") {
      await getAllUsers(dispatch);
    } else {
      const paramsObject = { text: searchTextFilter };
      await getUsersFiltered(dispatch, paramsObject);
    }
  };

  return (
    <div className={cx(styles["users-headers"])}>
      <Row>
        <Col xs="9">
          <FormGroup>
            <Input
              className={cx(styles["padding-filter"])}
              value={searchTextFilter}
              onChange={(e) => {
                onSearchTextChange(e.target.value);
              }}
              type="text"
              name="text"
              placeholder="Nombre o Usuario"
            />
          </FormGroup>
        </Col>
        <Col xs="3">
          <Button
            className={cx(styles["padding-search-button"])}
            onClick={onSubmit}
          >
            BUSCAR
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default UserFilter;
