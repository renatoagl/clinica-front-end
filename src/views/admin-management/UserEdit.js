import React, { useEffect } from "react";
import { getUser, getSpecialities } from "state/admin";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "state";
import UserForm from "./components/UserForm";

const UserEdit = (props) => {
  const { user } = useSelector("users");
  const dispatch = useDispatch();
  const { id } = useParams();

  useEffect(() => {
    const getUserData = async () => {
      await getUser(dispatch, id);
    };
    getUserData();
  }, []);

  useEffect(() => {
    const getSpecialitiesDescriptions = async () => {
      await getSpecialities(dispatch);
    };
    getSpecialitiesDescriptions();
  }, []);

  return <div>{user ? <UserForm id={id} /> : <div> Loading... </div>}</div>;
};

export default UserEdit;
