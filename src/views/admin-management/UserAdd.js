import React, { useEffect } from "react";
import { useSelector, useDispatch } from "state";
import { useHistory } from "react-router-dom";
import UserForm from "./components/UserForm";
import { getSpecialities } from "state/admin";

const UserAdd = (props) => {
  const { user, savedUser } = useSelector("users");
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    const buildEmptyForm = async () => {
      dispatch({ type: "BUILD_EMPTY_USER" });
    };
    buildEmptyForm();
  }, []);

  useEffect(() => {
    if (savedUser) {
      dispatch({ type: "RESET_SAVED_USER" });
      history.push(`user-edit/${user.id}`);
    }
  }, [savedUser]);

  useEffect(() => {
    const getSpecialitiesDescriptions = async () => {
      await getSpecialities(dispatch);
    };
    getSpecialitiesDescriptions();
  }, []);

  return (
    <div>
      {!user || !user.id ? <UserForm id={null} /> : <div> Loading... </div>}
    </div>
  );
};

export default UserAdd;
