import React from "react";
import cx from "classnames";
import styles from "./LoginScreen.module.scss";
import { Button } from "reactstrap";
import logo from "../../assets/images/left-sidebar-logo-color.png";
import { useIntl } from "react-intl";

const LoginScreen = () => {
  const { formatMessage: f } = useIntl();
  return (
    <>
      <div className={cx(styles["login-container"])}>
        <div className={cx(styles["login-card"])}>
          <div className={cx(styles["login-title"])}>
            <img src={logo} />
          </div>
          <div className={cx(styles["login-content"])}>
            <h2>{f({ id: "app.login.title" })}</h2>
            <div className={cx(styles["login-form"])}>
              <div className={cx(styles["form-top"])}>
                <label className={cx(styles["form-label"])}>
                  {f({ id: "app.user" })}
                </label>
                <br />
                <input
                  type="text"
                  className={cx(styles["form-field"])}
                  name="user"
                  required
                />
              </div>
              <div className={cx(styles["form-bottom"])}>
                <label className={cx(styles["form-label"])}>
                  {f({ id: "app.password" })}
                </label>
                <br />
                <input
                  type="password"
                  className={cx(styles["form-field"])}
                  name="password"
                  required
                />
                <div className={cx(styles["login-text-password"])}>
                  <a href="#" className={cx(styles["login-text"])}>
                    {f({ id: "app.password.forgotten" })}
                  </a>
                </div>
              </div>
            </div>
            <div className={cx(styles["button-opacity"])}>
              <Button className={cx(styles["login-button"])}>
                {f({ id: "app.login" })}
              </Button>
            </div>
          </div>
          <div className={cx(styles["login-password"])}>
            <a href="#" className={cx(styles["login-text"])}>
              {f({ id: "app.register" })}
            </a>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginScreen;
