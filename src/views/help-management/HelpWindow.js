import React from "react";
import cx from "classnames";
import styles from "./HelpWindow.module.scss";
import { Row, Col } from "reactstrap";
import book from "../../assets/images/book.png";
import email from "../../assets/images/email.png";
import key from "../../assets/images/key.png";
import NotificationManager from "react-notifications/lib/NotificationManager";
import Manual from "../../assets/documents/Manual.pdf";
import { post, get, put, deleteMethod } from "../../api/client";

const HelpWindow = () => {
  const downloadManual = async () => {
    try {
      await get(`reports/download-manual`);
      NotificationManager.success("Manual descargado correctamente.");
    } catch (error) {
      console.log(error);
    }
    console.log("Descargando...");
  };

  return (
    <div className={cx(styles["flex-container"])}>
      <div className={cx(styles["row-container"])}>
        <div className={cx(styles["row-left"])}>
          <img src={book} />
        </div>
        <div className={cx(styles["row-right"])}>
          <span>Guía:</span>
          <br></br>
          <a href={Manual} target="_blank">
            Manual de usuario
          </a>
        </div>
      </div>
      <div className={cx(styles["row-container"])}>
        <div className={cx(styles["row-left"])}>
          <img src={email} />
        </div>
        <div className={cx(styles["row-right"])}>
          <span>Correo electrónico de contacto: </span>
          <br></br>
          <a href="https://www.gmail.com/" target="_blank">
            renato.co2908@gmail.com
          </a>
        </div>
      </div>
      <div className={cx(styles["row-container"])}>
        <div className={cx(styles["row-left"])}>
          <img src={key} />
        </div>
        <div className={cx(styles["row-right"])}>
          <span>Licencia:</span>
          <br></br>
          <span className={cx(styles["gray-text"])}>
            Pagado y disponible para distintos equipos.
          </span>
          <br></br>
          <span>Validez de la licencia:</span>
          <br></br>
          <span className={cx(styles["gray-text"])}>Completa.</span>
          <br></br>
          <span>Versión del producto:</span>
          <br></br>
          <span className={cx(styles["gray-text"])}>v1.0.0</span>
        </div>
      </div>
    </div>
  );
};

export default HelpWindow;
