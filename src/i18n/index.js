import locale_en from "./locales/en.json";
import locale_es from "./locales/es.json";

export const locales = {
  'es': locale_es,
  'en': locale_en
};
