import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "remixicon/fonts/remixicon.css";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { StoreProvider } from "state";
import { IntlProvider } from "react-intl";
import { locales } from "./i18n";

ReactDOM.render(
  <React.StrictMode>
    <StoreProvider>
      <IntlProvider locale={"es"} messages={locales["es"]} defaultLocale="es">
        <App />
      </IntlProvider>
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
